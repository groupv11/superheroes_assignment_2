# Java Assignment 2

## Contents

[Information](#Information)

[Install](#Install)

[Usage](#Usage)

[Maintainers](#Maintainers)

[Licence](#Licence) 


## Information 

This is an application used for performing changes and queries in Postgres Database. Appendix A of the assignment is located in the 'scripts'-package.


### Folder structure
```
src
.
|-- main
|   |-- java
|   |   `-- com
|   |       `-- example
|   |           `-- assignment_2
|   |               |-- Assignment2Application.java
|   |               |-- ChinookAppRunner.java
|   |               |-- Models
|   |               |   |-- Customer.java
|   |               |   |-- CustomerCountry.java
|   |               |   |-- CustomerGenre.java
|   |               |   `-- CustomerSpender.java
|   |               `-- Repositories
|   |                   |-- CRUDRepository.java
|   |                   |-- CustomerRepository.java
|   |                   `-- CustomerRepositoryImpl.java
|   `-- resources
|       `-- application.properties
`-- test
    `-- java
        `-- com
            `-- example
                `-- assignment_2
                    `-- Assignment2ApplicationTests.java

```


## Install
Clone git repo - open in IDE
Open pgAdmin and create database chinook_db.
Upload the script found in chinook_db folder in this project


## Usage
run 'Assignment2Application.java' for a short demonstration. 
run 'Assignment2ApplicationTests.java' for tests.

## Maintainers
Karoline Øijorden, Ulrik Lunde and Vegard Gunnarson

## Licence
Open
