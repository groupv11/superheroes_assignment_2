package com.example.assignment_2.Models;

public record CustomerSpender (int customerID, int total) {
    @Override
    public String toString() {
        return "Sum: " + total;
    }
}
