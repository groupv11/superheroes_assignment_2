package com.example.assignment_2.Models;


import java.util.List;

public record CustomerGenre(List<String> genres)  {
    @Override
    public String toString() {
        String allGenres = "Genres: ";
        for (String genre: genres) {
            allGenres += genre + " ";
        }
        return allGenres;
    }

}
