package com.example.assignment_2.Models;

public record CustomerCountry(String country) {

    @Override
    public String toString() {
        return "Country: "+country;

    }

}
