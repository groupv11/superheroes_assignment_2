package com.example.assignment_2.Models;



public record Customer(String first_name, String last_name, String country, String postal_code, String phone, String email){

    @Override
    public String toString(){

        return "First name: " + first_name + ", Last name: " + last_name + ", Country: " + country + ", Postal Code: "
                + postal_code + ", Phone: " + phone + ", Email: " + email ;

    }
}
