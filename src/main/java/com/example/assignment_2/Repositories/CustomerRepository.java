package com.example.assignment_2.Repositories;

import com.example.assignment_2.Models.Customer;
import com.example.assignment_2.Models.CustomerCountry;
import com.example.assignment_2.Models.CustomerGenre;
import com.example.assignment_2.Models.CustomerSpender;

import java.sql.SQLException;
import java.util.List;
/**
 * @Authors
 * Karoline Øijorden, Ulrik Lunde,Vegard Gunnarson
 */
public interface CustomerRepository extends CRUDRepository<Customer, Integer>{

      /**
       * Get customer from given name
       * @param name any existing name
       * @return customer
       */
      Customer getCustomerByName(String name) throws SQLException;

      /**
       * Get list of all instances in table
       * @return all rows
       */
      List<Customer> findAll() throws SQLException;

      /**
       * Returns a page with limited rows
       * @param limit amount of rows
       * @param offset amount of rows to skip
       * @return rows
       */
      List<Customer> getPageOfCustomers(int limit, int offset) throws SQLException;

      /**
       * Inserts customer to database
       * @param customer any customer
       */
      void insert(Customer customer) ;

      /**
       * Returns customer given an id
       * @param id any existing id
       * @return Customer
       */
      Customer findById(int id) throws SQLException;

      /**
       * Updates a given customer by id. The first parameter is the id of the customer you want to update, the second parameter is what you want to update
       * (can be any of the columns from the database), the last parameter is the value you want to insert into the chosen column
       * @param id any valid int
       * @param column any valid column
       * @param value any valid value
       */
      void update(int id, String column, String value);

      /**
       * Used for testing connection to postgres database
       */
      void test();


      /**
       * Returns country with most customer
       * @return
       */
      CustomerCountry getMaxCountry() throws SQLException;

      /**
       * Returns the highest spending customer
       * @return
       */
      CustomerSpender getHighestSpender() throws SQLException;

      /**
       * For a given customer, get their most popular genre, in the case of a tie, display both.
       * @param id any valid int
       * @return CustomerGenre
       */
      CustomerGenre getMostPopularGenre(int id) throws SQLException;

}
