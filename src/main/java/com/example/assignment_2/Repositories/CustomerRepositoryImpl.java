package com.example.assignment_2.Repositories;

import com.example.assignment_2.Models.Customer;
import com.example.assignment_2.Models.CustomerCountry;
import com.example.assignment_2.Models.CustomerGenre;
import com.example.assignment_2.Models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Authors
 * Karoline Øijorden, Ulrik Lunde,Vegard Gunnarson
 */
@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public void test() {
        try(Connection conn = DriverManager.getConnection(url, username,password);) {
            System.out.println("Connected to Postgres...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public  List<Customer> findAll() throws SQLException {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email  FROM customer";
        ResultSet result = connectToDatabase(sql);
        List<Customer> customers = new ArrayList<>();
        Customer c1 = null;
        while(result.next()) {
            c1 = new Customer(
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email"));
            customers.add(c1);
        }
        return customers;
    }

    @Override
    public Customer findById(int id) throws SQLException {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email" +
                "  FROM customer WHERE customer_id =" + id;
        ResultSet result = connectToDatabase(sql);
        Customer c1 = null;
        if(result.next()) {
            c1 = new Customer(
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email"));
            return c1;
        }
        return c1;
    }

    @Override
    public Customer getCustomerByName(String name) throws SQLException{
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email" +
                    "  FROM customer WHERE first_name = '" + name + "'";
        ResultSet result = connectToDatabase(sql);
        Customer c1 = null;
        if(result.next()) {
            c1 = new Customer(
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email"));
            return c1;
        }
        return c1;
    }

    @Override
    public List<Customer> getPageOfCustomers(int limit, int offset) throws SQLException {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email  " +
                    "FROM customer LIMIT " + limit + " OFFSET " + offset;
        ResultSet result = connectToDatabase(sql);
        Customer c1 = null;
        List<Customer> customers = new ArrayList<>();
        while(result.next()) {
            c1 = new Customer(
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email"));
            customers.add(c1);
        }
        return customers;
    }

    @Override
    public void insert(Customer customer){
        String sql = "INSERT INTO customer(first_name,last_name,country,postal_code,phone,email) " +
                "VALUES ('"+customer.first_name()+"','"+customer.last_name()+"','"+customer.country()+"','"
                +customer.postal_code()+"','"+customer.phone()+"','"+customer.email()+"')";
        connectToDatabaseInsert(sql);
    }

    @Override
    public void update(int id, String column, String value){
        String sql = "UPDATE customer SET "+column+" = '"+value+"' WHERE customer_id="+id;
        connectToDatabaseInsert(sql);
    }

    @Override
    public CustomerCountry getMaxCountry() throws SQLException {
        String sql = "SELECT country FROM customer GROUP BY 1 ORDER BY count(*) DESC LIMIT 1";
        CustomerCountry country = null;
        ResultSet result = connectToDatabase(sql);
        if(result.next()) {
            country = new CustomerCountry(result.getString("country"));
            return country;
        }
        return country;
    }

    @Override
    public CustomerSpender getHighestSpender() throws SQLException {
        String sql = "SELECT customer_id, SUM(total) FROM invoice GROUP BY customer_id ORDER BY sum DESC LIMIT 1";
        ResultSet result = connectToDatabase(sql);
        CustomerSpender c1 = null;
        if (result.next()) {
            c1 = new CustomerSpender(
                    result.getInt("customer_id"),
                    result.getInt("sum"));
        }
        return c1;
    }

    @Override
    public CustomerGenre getMostPopularGenre(int customerID) throws SQLException{
        String sql = """
                SELECT genre.Name
                FROM genre
                INNER JOIN track ON genre.genre_id = track.genre_id
                INNER JOIN invoice_line ON track.track_id = invoice_line.track_id
                INNER JOIN invoice ON invoice_line.invoice_id = invoice.invoice_id\s
                WHERE customer_id=15
                GROUP BY 1 ORDER BY count(*) DESC
                FETCH FIRST 1 ROWS WITH TIES;""";
        ResultSet result = connectToDatabase(sql);
        List<String> genre = new ArrayList<String>();
        while (result.next()) {
            genre.add(result.getString("name"));
        }
        return new CustomerGenre(genre);
    }

    /**
     * Connects to database and runs sql query
     * @param sql any sql query
     */
    private void connectToDatabaseInsert(String sql) {
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Connects to database and runs sql query
     * @param sql any sql query
     */
    private ResultSet connectToDatabase(String sql) {
        ResultSet result = null;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            result = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
