package com.example.assignment_2.Repositories;

import java.sql.SQLException;
import java.util.List;

public interface CRUDRepository<T, U> {
    List<T> findAll() throws SQLException;
    T findById(int id) throws SQLException;
    void insert(T object);
    void update(int id, String column, String value);

    void test();
}
