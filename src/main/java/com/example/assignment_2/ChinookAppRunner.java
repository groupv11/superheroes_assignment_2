package com.example.assignment_2;

import com.example.assignment_2.Models.Customer;
import com.example.assignment_2.Models.CustomerCountry;
import com.example.assignment_2.Models.CustomerGenre;
import com.example.assignment_2.Models.CustomerSpender;
import com.example.assignment_2.Repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Authors
 * Karoline Øijorden, Ulrik Lunde,Vegard Gunnarson
 */

@Component
public class ChinookAppRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public ChinookAppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        customerRepository.test();

        System.out.println("List of all customers:");
        List<Customer> customerList = customerRepository.findAll();
        for(int i = 0; i < customerList.toArray().length; i++){
            System.out.println(customerList.get(i).toString());
        }


        System.out.println("Country with the most customers:");
        CustomerCountry c = customerRepository.getMaxCountry();
        System.out.println(c.toString());


        System.out.println("Highest spending customer:");
        CustomerSpender c2 = customerRepository.getHighestSpender();
        System.out.println("Sum: "+c2.total() +". " +  customerRepository.findById(c2.customerID()));

        System.out.println("Updating customer_id 60 with new name: Woolrich...");
        customerRepository.update(60, "first_name", "Woolrich");


        System.out.println("Most popular genre:");
        CustomerGenre genre = customerRepository.getMostPopularGenre(1);
        System.out.println(genre.toString());


        System.out.println("Adding new customer, Ulrik Nordmann..");
        Customer customer = new Customer( "Ulrik", "Caroline", "USA", "6700", "12365423", "Vegard@ulrik.com");
        customerRepository.insert(customer);

        System.out.println("Getting customer with name Ulrik");
        System.out.println(customerRepository.getCustomerByName("Ulrik").toString());

        System.out.println("Getting customer with id 25:");
        System.out.println(customerRepository.findById(25).toString());


        System.out.println("Printing all customers with offset 20 and limit 2..");
        customerList = customerRepository.getPageOfCustomers(2, 20);
        for(int i = 0; i < customerList.toArray().length; i++){
            System.out.println(customerList.get(i).toString());
        }


    }
}
