package com.example.assignment_2;

import com.example.assignment_2.Models.*;
import com.example.assignment_2.Repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Assignment2ApplicationTests {

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    //Add customer to database, then retrieve the same customer to check that it was inserted.
    void InsertCustomer_RetrieveSameCustomer(){

        //Arrange
        Customer customer = new Customer("Janmagne", "Nordmann", "Norway", "6700", "12464378", "Ola@hotmail.com");
        String expected = "12464378";
        Customer c1 = null;

        //Act
        customerRepository.insert(customer);
        try {
            c1 = customerRepository.getCustomerByName("Janmagne");
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }

        //Assert
         assertEquals(expected, c1.phone());


    }

    //Retrieve a customer by id
    @Test
    void getCustomerById_ReturnsCustomerWithGivenId(){

        //Arrange
        String expected = "Bjørn";
        Customer c1 = null;

        //Act
        try {
            c1 = customerRepository.findById(4);
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }

        //Assert
        assertEquals(expected, c1.first_name());
    }

    //Check if customer got updated
    @Test
    void updateCustomer_getCustomerById_CheckIfUpdated(){
        //Arrange
        String expected = "111";
        Customer c1 = null;

        //Act
        customerRepository.update(10, "phone", "111");

        try {
            c1 = customerRepository.findById(10);
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }

        //Assert
        assertEquals(expected, c1.phone());

    }

    @Test
    void testMaxCountry_returnsCountryWithMostCustomers(){

        //Arrange
        CustomerCountry actual = null;

        //Act
        try {
            actual = customerRepository.getMaxCountry();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        CustomerCountry expected = new CustomerCountry("USA");

        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void testMaxSpender_returnsHighestSpendingCustomer(){

        //Arrange
        int actual = 0;

        //Act
        try {
            actual = customerRepository.getHighestSpender().total();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        int expected = 49;

        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void testPopularGenre() {

        //Arrange
        CustomerGenre actual = null;

        //Act
        try {
            actual = customerRepository.getMostPopularGenre(1);
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        String expected = "Genres: Rock ";

        //Assert
        assertEquals(expected,actual.toString());

    }



}
