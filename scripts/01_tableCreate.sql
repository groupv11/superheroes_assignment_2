DROP TABLE IF EXISTS superhero;
CREATE TABLE superhero (
id serial PRIMARY KEY,
name varchar(50) NOT NULL,
alias varchar(50),
origin varchar(200)
);

DROP TABLE IF EXISTS assistant;
CREATE TABLE assistant (
id serial PRIMARY KEY,
name varchar(50) NOT NULL
);

DROP TABLE IF EXISTS power;
CREATE TABLE power (
id serial PRIMARY KEY,
name varchar(50) NOT NULL,
description varchar(200)
);