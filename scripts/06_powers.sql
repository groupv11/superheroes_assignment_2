INSERT INTO power("name",description)
VALUES('Run','Ability to run up to 5km/t');
INSERT INTO power("name",description)
VALUES('Fly','Ability to fly');
INSERT INTO power("name",description)
VALUES('Swim','Ability to swim');
INSERT INTO power("name",description)
VALUES('Jump','Ability to jump (up)');


INSERT INTO superhero_power(superhero_id,power_id) 
VALUES (1,2);
INSERT INTO superhero_power(superhero_id,power_id) 
VALUES (1,3);
INSERT INTO superhero_power(superhero_id,power_id) 
VALUES (2,2);
INSERT INTO superhero_power(superhero_id,power_id) 
VALUES (3,3);

