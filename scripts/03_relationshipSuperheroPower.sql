DROP TABLE IF EXISTS superhero_power;

CREATE TABLE superhero_power(
superhero_id INT,
power_id INT,
PRIMARY KEY (superhero_id,power_id),
CONSTRAINT fk_superhero_id FOREIGN KEY (superhero_id)
REFERENCES superhero(id)
ON UPDATE CASCADE
ON DELETE CASCADE,
CONSTRAINT fk_power_id foreign key (power_id)
REFERENCES power(id)
ON UPDATE CASCADE
ON DELETE CASCADE
);